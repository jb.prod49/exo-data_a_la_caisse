-- Créer un produit “Truc”, avec comme description “Lorem Ipsum” comme image “https://picsum.photos/200”, qui coute 200 €.

INSERT INTO name (name) VALUES ('Truc');
SET @name_id = LAST_INSERT_ID();
INSERT INTO describ (description) VALUES ('Lorem Ipsum');
SET @describ_id = LAST_INSERT_ID();
INSERT INTO image (link_img) VALUES ('https://picsum.photos/200');
SET @image_id = LAST_INSERT_ID();
INSERT INTO price (price) VALUES (200);
SET @price_id = LAST_INSERT_ID();

INSERT INTO product (id_name, id_describ, id_image, id_price, id_available)
VALUES (@name_id, @describ_id, @image_id, @price_id, 1);

SELECT
    p.id AS product_id,
    n.name AS product_name,
    d.description AS product_description,
    i.link_img AS product_image,
    pr.price AS product_price
FROM
    product p
JOIN
    name n ON p.id_name = n.id
JOIN
    describ d ON p.id_describ = d.id
JOIN
    image i ON p.id_image = i.id
JOIN
    price pr ON p.id_price = pr.id
WHERE
    n.name = 'Truc';

-- Créer un produit “Bidule”, avec comme description “Lorem merol” comme image “https://picsum.photos/100”, qui coute 500 €.

INSERT INTO name (name) VALUES ('Bidule');
SET @name_id = LAST_INSERT_ID();
INSERT INTO describ (description) VALUES ('Lorem merol');
SET @describ_id = LAST_INSERT_ID();
INSERT INTO image (link_img) VALUES ('https://picsum.photos/100');
SET @image_id = LAST_INSERT_ID();
INSERT INTO price (price) VALUES (500);
SET @price_id = LAST_INSERT_ID();

INSERT INTO product (id_name, id_describ, id_image, id_price, id_available)
VALUES (@name_id, @describ_id, @image_id, @price_id, 1);

SELECT
    p.id AS product_id,
    n.name AS product_name,
    d.description AS product_description,
    i.link_img AS product_image,
    pr.price AS product_price
FROM
    product p
JOIN
    name n ON p.id_name = n.id
JOIN
    describ d ON p.id_describ = d.id
JOIN
    image i ON p.id_image = i.id
JOIN
    price pr ON p.id_price = pr.id
WHERE
    n.name = 'Bidule';

-- Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.

SELECT
    p.id AS product_id,
    n.name AS product_name,
    d.description AS product_description,
    i.link_img AS product_image,
    pr.price AS product_price
FROM
    product p
JOIN
    name n ON p.id_name = n.id
JOIN
    describ d ON p.id_describ = d.id
JOIN
    image i ON p.id_image = i.id
JOIN
    price pr ON p.id_price = pr.id;

-- Récupérer Truc à partir de son id et vérifier sa description et son image.

SELECT
    p.id AS product_id,
    n.name AS product_name,
    d.description AS product_description,
    i.link_img AS product_image,
    pr.price AS product_price
FROM
    product p
JOIN
    name n ON p.id_name = n.id
JOIN
    describ d ON p.id_describ = d.id
JOIN
    image i ON p.id_image = i.id
JOIN
    price pr ON p.id_price = pr.id
WHERE 
    n.id = 11;

-- Créer une nouvelle commande.

INSERT INTO orders (id_client, delivery_address, billing_address, id_payment_method, id_order_state)
VALUES (NULL, NULL, NULL, 'credit_card', 'cart');

-- Sélectionner la nouvelle commande
SELECT * FROM orders WHERE id = LAST_INSERT_ID();

-- Ajouter 3 Trucs à la commande.
INSERT INTO order_line (id_product, id_order, quantity)
VALUES (11, 11, 3);

SELECT
    ol.id_product AS product_id,
    n.name AS product_name,
    ol.quantity
FROM
    order_line ol
JOIN
    product p ON ol.id_product = p.id
JOIN
    name n ON p.id_name = n.id
WHERE
    ol.id_order = 11;

-- Ajouter 1 Bidules à la commande.

INSERT INTO order_line (id_product, id_order, quantity)
VALUES (12, 11, 1);

SELECT
    ol.id_product AS product_id,
    n.name AS product_name,
    ol.quantity
FROM
    order_line ol
JOIN
    product p ON ol.id_product = p.id
JOIN
    name n ON p.id_name = n.id
WHERE
    ol.id_order = 11;

-- Créer le client “Bob” avec comme mail “bob@gmail.com”.

INSERT INTO identity (email, first_name)
VALUES ('bob@gmail.com', 'Bob');
SET @bob_identity_id = LAST_INSERT_ID();

INSERT INTO client (id_identity)
VALUES (@bob_identity_id);

SELECT
    c.id AS client_id,
    i.email,
    i.first_name,
    i.last_name,
    i.phone
FROM
    client c
JOIN
    identity i ON c.id_identity = i.id
WHERE
    i.email = 'bob@gmail.com';

-- Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.

UPDATE identity
SET phone = '0123456789'
WHERE email = 'bob@gmail.com';

SELECT
    c.id AS client_id,
    i.email,
    i.first_name,
    i.last_name,
    i.phone
FROM
    client c
JOIN
    identity i ON c.id_identity = i.id
WHERE
    i.email = 'bob@gmail.com';

-- Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).

UPDATE orders SET id_order_state = 'nouvel_etat' WHERE id = 11;
SELECT * FROM orders WHERE id = 11;

-- Créer l’adresse “12 street St, 12345 Schenectady, New York, US”

INSERT INTO street (name_street) VALUES ('12 street St')
ON DUPLICATE KEY UPDATE id=id;
SET @street_id = LAST_INSERT_ID();


INSERT INTO city (name_city, zipcode) VALUES ('Schenectady', '12345')
ON DUPLICATE KEY UPDATE id=id;
SET @city_id = LAST_INSERT_ID();

INSERT INTO state (name_state) VALUES ('New York')
ON DUPLICATE KEY UPDATE id=id;
SET @state_id = LAST_INSERT_ID();

INSERT INTO country (name_country) VALUES ('US')
ON DUPLICATE KEY UPDATE id=id;
SET @country_id = LAST_INSERT_ID();

INSERT INTO address (id_street, id_city, id_state, id_country)
VALUES (@street_id, @city_id, @state_id, @country_id)
ON DUPLICATE KEY UPDATE id=id;
SET @address_id = LAST_INSERT_ID();

SELECT
    a.id AS address_id,
    s.name_street AS street,
    c.name_city AS city,
    c.zipcode AS zipcode,
    st.name_state AS state,
    co.name_country AS country
FROM
    address a
JOIN
    street s ON a.id_street = s.id
JOIN
    city c ON a.id_city = c.id
JOIN
    state st ON a.id_state = st.id
JOIN
    country co ON a.id_country = co.id
WHERE
    a.id = @address_id;

-- Essayer d’associer cette adresse comme facturation à la commande.

SET @billingAddressId = (SELECT default_address FROM client WHERE id = 11);

UPDATE orders
SET billing_address = @billingAddressId
WHERE id = 11;

SELECT * FROM orders WHERE id = 11;

-- Associer cette adresse à Bob.
INSERT INTO client_address (id_client, id_address)
VALUES (11, 11);

UPDATE client
SET default_address = 1
WHERE id = 11;

SELECT * FROM client WHERE id = 11;

-- Associer cette adresse comme facturation à la commande.

SET @billingAddressId = (SELECT default_address FROM client WHERE id = 11);

UPDATE orders
SET billing_address = @billingAddressId
WHERE id = 11;

SELECT * FROM orders WHERE id = 11;
-- Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”

INSERT INTO street (name_street) VALUES ('12 boulevard de Strasbourg')
ON DUPLICATE KEY UPDATE id=id;
SET @street_id = LAST_INSERT_ID();


INSERT INTO city (name_city, zipcode) VALUES ('Toulouse', '31000')
ON DUPLICATE KEY UPDATE id=id;
SET @city_id = LAST_INSERT_ID();

INSERT INTO state (name_state) VALUES ('OC')
ON DUPLICATE KEY UPDATE id=id;
SET @state_id = LAST_INSERT_ID();

INSERT INTO country (name_country) VALUES ('France”')
ON DUPLICATE KEY UPDATE id=id;
SET @country_id = LAST_INSERT_ID();

INSERT INTO address (id_street, id_city, id_state, id_country)
VALUES (@street_id, @city_id, @state_id, @country_id)
ON DUPLICATE KEY UPDATE id=id;
SET @address_id = LAST_INSERT_ID();

SELECT
    a.id AS address_id,
    s.name_street AS street,
    c.name_city AS city,
    c.zipcode AS zipcode,
    st.name_state AS state,
    co.name_country AS country
FROM
    address a
JOIN
    street s ON a.id_street = s.id
JOIN
    city c ON a.id_city = c.id
JOIN
    state st ON a.id_state = st.id
JOIN
    country co ON a.id_country = co.id
WHERE
    a.id = @address_id;

-- Associer cette adresse à Bob.

INSERT INTO client_address (id_client, id_address)
VALUES (11, 12);

UPDATE client
SET default_address = 2
WHERE id = 11;

SELECT * FROM client WHERE id = 11;

-- Associer cette adresse comme adresse de livraison.

SET @delivery_addressId = (SELECT default_address FROM client WHERE id = 11);

UPDATE orders
SET delivery_address = @delivery_addressId
WHERE id = 11;

SELECT * FROM orders WHERE id = 11;

-- Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).

-- Associer le moyen de paiement “Card” à la commande.
UPDATE orders
SET id_payment_method = 'Card'
WHERE id = 11;

SELECT * FROM orders WHERE id = 11;
-- Passer la commande dans l’état suivant.

-- Passer la commande dans l’état suivant.

-- Vérifier que la commande est dans l’état “sent”.

-- Récupérer la liste des adresses de Bob.

SELECT
    a.id AS address_id,
    s.name_street AS street,
    ci.name_city AS city,
    ci.zipcode AS zipcode,
    st.name_state AS state,
    co.name_country AS country
FROM
    client_address ca
JOIN
    client c ON ca.id_client = c.id
JOIN
    address a ON ca.id_address = a.id
JOIN
    street s ON a.id_street = s.id
JOIN
    city ci ON a.id_city = ci.id
JOIN
    state st ON a.id_state = st.id
JOIN
    country co ON a.id_country = co.id
WHERE
    c.id = 11;

-- Récupérer la liste des commandes de Bob.
SELECT
    o.id AS order_id,
    os.state AS order_state,
    pm.method AS payment_method,
    o.date_statut
FROM
    orders o
JOIN
    Order_State os ON o.id_order_state = os.state
JOIN
    Payment_Method pm ON o.id_payment_method = pm.method
WHERE
    o.id_client = 11;