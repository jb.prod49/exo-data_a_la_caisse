import mysql.connector
from config.bdd_config import DB_CONFIG
from faker import Faker
fake = Faker()

#--------------------------------#
#--------connexion BDD ----------#
#--------------------------------#

conn = mysql.connector.connect(**DB_CONFIG)
cursor = conn.cursor()

#---------------------------------------------#
#-------- exécution du fichier .sql ----------#
#---------------------------------------------#

with open('init.sql', 'r') as file:
    sql_script = file.read()

sql_statements = sql_script.split(';')

for statement in sql_statements:
    if statement.strip():  
        cursor.execute(statement)

#-------------------------------------#
#-------- Ajout des clients ----------#
#-------------------------------------#
star_wars_clients = [
    {
        "email": "palpatine@example.com",
        "first_name": "Palpatine",
        "last_name": "Emperor",
        "phone": "123456789",
        "street": "Dark Side Street",
        "city": "Sith City",
        "zipcode": "66666",
        "state": "Dark Side State",
        "country": "Galactic Empire"
    },
    {
        "email": "vader@example.com",
        "first_name": "Darth",
        "last_name": "Vader",
        "phone": "987654321",
        "street": "Sith Lord Street",
        "city": "Death Star City",
        "zipcode": "99999",
        "state": "Dark Side State",
        "country": "Galactic Empire"
    },
    {
        "email": "luke@example.com",
        "first_name": "Luke",
        "last_name": "Skywalker",
        "phone": "111223344",
        "street": "Jedi Street",
        "city": "Rebel Base City",
        "zipcode": "11111",
        "state": "Light Side State",
        "country": "Rebel Alliance"
    },
    {
        "email": "leia@example.com",
        "first_name": "Leia",
        "last_name": "Organa",
        "phone": "555667788",
        "street": "Alderaan Avenue",
        "city": "Alderaan City",
        "zipcode": "54321",
        "state": "Light Side State",
        "country": "Rebel Alliance"
    },
    {
        "email": "c3po@example.com",
        "first_name": "C-3PO",
        "last_name": None,
        "phone": "123987654",
        "street": "Droid Street",
        "city": "Droid City",
        "zipcode": "78901",
        "state": "Droid State",
        "country": "Galactic Republic"
    },
    {
        "email": "r2d2@example.com",
        "first_name": "R2-D2",
        "last_name": None,
        "phone": "456789012",
        "street": "Astro Droid Street",
        "city": "Astro Droid City",
        "zipcode": "23456",
        "state": "Droid State",
        "country": "Galactic Republic"
    },
    {
        "email": "han@example.com",
        "first_name": "Han",
        "last_name": "Solo",
        "phone": "678901234",
        "street": "Smuggler Street",
        "city": "Millennium Falcon City",
        "zipcode": "56789",
        "state": "Outer Rim State",
        "country": "Independent Systems"
    },
    {
        "email": "yoda@example.com",
        "first_name": "Yoda",
        "last_name": None,
        "phone": "987654321",
        "street": "Dagobah Street",
        "city": "Dagobah City",
        "zipcode": "87654",
        "state": "Swampy State",
        "country": "Unknown Regions"
    },
    {
        "email": "chewie@example.com",
        "first_name": "Chewbacca",
        "last_name": None,
        "phone": "654321098",
        "street": "Wookiee Street",
        "city": "Kashyyyk City",
        "zipcode": "32109",
        "state": "Wookiee State",
        "country": "Wookiee Homeworld"
    }
]

def insert_client(data):
    try:
        # Insertion des données d'identité
        cursor.execute("""
            INSERT INTO identity (email, first_name, last_name, phone)
            VALUES (%s, %s, %s, %s)
        """, (data["email"], data["first_name"], data["last_name"], data["phone"]))

        identity_id = cursor.lastrowid

        # Insertion des données de la rue (street)
        cursor.execute("""
            INSERT INTO street (name_street)
            VALUES (%s)
        """, (data["street"],))

        street_id = cursor.lastrowid

        # Insertion des données de la ville (city)
        cursor.execute("""
            INSERT INTO city (name_city, zipcode)
            VALUES (%s, %s)
        """, (data["city"], data["zipcode"]))

        city_id = cursor.lastrowid

        # Insertion des données de l'état (state)
        cursor.execute("""
            INSERT INTO state (name_state)
            VALUES (%s)
        """, (data["state"],))

        state_id = cursor.lastrowid

        # Insertion des données du pays (country)
        cursor.execute("""
            INSERT INTO country (name_country)
            VALUES (%s)
        """, (data["country"],))

        country_id = cursor.lastrowid

        # Insertion des données d'adresse
        cursor.execute("""
            INSERT INTO address (id_street, id_city, id_state, id_country)
            VALUES (%s, %s, %s, %s)
        """, (street_id, city_id, state_id, country_id))

        address_id = cursor.lastrowid

        # Insertion des données de client
        cursor.execute("""
            INSERT INTO client (id_identity, default_address)
            VALUES (%s, %s)
        """, (identity_id, address_id))

        # Insertion de la relation client_adresse
        cursor.execute("""
            INSERT INTO client_address (id_client, id_address)
            VALUES (%s, %s)
        """, (identity_id, address_id))

        print(f"Client {data['first_name']} {data['last_name']} ajouté avec succès!")

    except Exception as e:
        # Annulation en cas d'erreur
        conn.rollback()
        print(f"Erreur lors de l'ajout du client {data['first_name']} {data['last_name']} : {str(e)}")

# Ajout des clients Star Wars
for client_data in star_wars_clients:
    insert_client(client_data)

# Validation des modifications
conn.commit()

# Fermeture des connexions
cursor.close()
conn.close()


#---------------------------------#
#-------- Ajout adresse ----------#
#---------------------------------#
exit()
# Fonction pour obtenir un ID aléatoire à partir d'une table donnée
def get_random_id(table_name):
    cursor.execute(f"SELECT id FROM {table_name} ORDER BY RAND() LIMIT 1")
    row = cursor.fetchone()
    if row:
        return row[0]
    return None

# Ajout de données dans la table client
for _ in range(10):
    random_email = fake.email()
    random_first_name = fake.first_name()
    random_last_name = fake.last_name()
    random_phone = fake.phone_number()

    cursor.execute("""
        INSERT IGNORE INTO identity (email, first_name, last_name, phone)
        VALUES (%s, %s, %s, %s)
    """, (random_email, random_first_name, random_last_name, random_phone))

    # Utilisez LAST_INSERT_ID() pour récupérer l'ID de l'identité insérée ou existante
    cursor.execute("SELECT LAST_INSERT_ID()")

    identity_id = cursor.fetchone()[0]

    cursor.execute("""
        INSERT IGNORE INTO client (default_address, id_identity)
        VALUES (%s, %s)
    """, (get_random_id("address"), identity_id))

# Ajout de données dans la table orders
for _ in range(10):
    random_client_id = get_random_id("client")
    random_delivery_address = get_random_id("address")
    random_billing_address = get_random_id("address")

    cursor.execute("""
        INSERT INTO orders (id_client, delivery_address, billing_address)
        VALUES (%s, %s, %s)
    """, (random_client_id, random_delivery_address, random_billing_address))

# Ajout de données dans la table product
for _ in range(10):
    random_name = fake.word()
    random_description = fake.text()
    random_image_link = fake.image_url()
    random_price = fake.random_int(min=1, max=100)
    random_available = fake.boolean()

    cursor.execute("""
        INSERT INTO name (name) VALUES (%s)
    """, (random_name,))

    cursor.execute("""
        INSERT INTO describ (description) VALUES (%s)
    """, (random_description,))

    cursor.execute("""
        INSERT INTO image (link_img) VALUES (%s)
    """, (random_image_link,))

    cursor.execute("""
        INSERT INTO price (price) VALUES (%s)
    """, (random_price,))

    cursor.execute("""
        INSERT INTO available (available) VALUES (%s)
    """, (random_available,))

    product_id = get_random_id("name")
    name_id = get_random_id("name")
    describ_id = get_random_id("describ")
    image_id = get_random_id("image")
    price_id = get_random_id("price")
    available_id = get_random_id("available")

    cursor.execute("""
        INSERT INTO product (id_name, id_describ, id_image, id_price, id_available)
        VALUES (%s, %s, %s, %s, %s)
    """, (name_id, describ_id, image_id, price_id, available_id))

# Ajout de données dans la table address
for _ in range(10):
    random_street = fake.street_address()
    random_city = fake.city()
    random_zipcode = fake.zipcode()
    random_state = fake.state()
    random_country = fake.country()

    cursor.execute("""
        INSERT INTO street (name_street) VALUES (%s)
    """, (random_street,))
    cursor.execute("""
        INSERT INTO city (name_city, zipcode) VALUES (%s, %s)
    """, (random_city, random_zipcode))
    cursor.execute("""
        INSERT INTO state (name_state) VALUES (%s)
    """, (random_state,))
    cursor.execute("""
        INSERT INTO country (name_country) VALUES (%s)
    """, (random_country,))

    street_id = get_random_id("street")
    city_id = get_random_id("city")
    state_id = get_random_id("state")
    country_id = get_random_id("country")

    cursor.execute("""
        INSERT INTO address (id_street, id_city, id_state, id_country)
        VALUES (%s, %s, %s, %s)
    """, (street_id, city_id, state_id, country_id))

# Commit et fermeture de la connexion
conn.commit()
conn.close()
