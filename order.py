# order.py
from enum import Enum
import mysql.connector
from fastapi import HTTPException, APIRouter
from pydantic import BaseModel
from config.bdd_config import DB_CONFIG

router = APIRouter()

# Connexion BDD
conn = mysql.connector.connect(**DB_CONFIG)

# Classes
class PaymentMethodEnum(str, Enum):
    none = 'none'
    credit_card = 'credit_card'
    bank_check_on_delivery = 'bank_check_on_delivery'
    cash = 'cash'

class OrderStateEnum(str, Enum):
    cart = 'cart'
    validated = 'validated'
    sent = 'sent'
    delivered = 'delivered'

class Order(BaseModel):
    id: int | None = None
    id_client: int | None = None
    delivery_address: int | None = None
    billing_address: int | None = None
    payment_method: PaymentMethodEnum | None = 'none'
    order_state: OrderStateEnum | None = 'cart'

# Créer une commande
@router.post("/order")
async def create_order(order: Order):
    cursor = conn.cursor(dictionary=True)
    try:
        
        cursor.execute(
            """
            INSERT INTO orders (id_client, delivery_address, billing_address, 
                                payment_method, order_state)
            VALUES (%s, %s, %s, %s, %s)
            """,
            (order.id_client, order.delivery_address, order.billing_address,
                order.payment_method, order.order_state)
        )
        new_order_id = cursor.lastrowid
        conn.commit()
        return {"order_id": new_order_id}

    except Exception as e:
        raise HTTPException(status_code=500, detail="Erreur lors de la création de la commande")
    finally:
        cursor.close()

# Retourne la commande avec toutes ses lignes, les adresses et le client.
@router.get("/order/{order_id}")
async def get_order(order_id: int):
    cursor = conn.cursor(dictionary=True)
    try:
        cursor.execute(
            """
            SELECT 
                o.id AS ID_order,
                o.id_client AS ID_client,
                c.id_identity,
                i.email,
                i.first_name,
                i.last_name,
                i.phone             
            FROM 
                orders o
            JOIN 
                client c ON o.id_client = c.id
            JOIN
                identity i ON c.id_identity = i.id
            WHERE
                o.id = %s
            """,
            (order_id,)
        )
        order_info_client = cursor.fetchall()

        if not order_info_client:
            raise HTTPException(status_code=404, detail="Commande non trouvée.")

        cursor.execute(
            """
            SELECT 
                o.delivery_address AS ID_delivery_address,
                st.name_street AS street,
                ci.name_city AS city,
                ci.zipcode AS zipcode,
                sta.name_state AS state,
                co.name_country AS country
            FROM 
                orders o
            JOIN 
                client c ON o.id_client = c.id
            JOIN
                identity i ON c.id_identity = i.id
            JOIN
                address a ON o.delivery_address = a.id
            JOIN
                street st ON  a.id_street = st.id
            JOIN 
                city ci ON a.id_city = ci.id
            JOIN
                state sta ON a.id_state = sta.id
            JOIN
                country co ON a.id_country = co.id
            WHERE
                o.id = %s
            """,
            (order_id,)
        )
        order_delivery_address = cursor.fetchall()

        if not order_delivery_address:
            raise HTTPException(status_code=404, detail="Adresse de livraison non trouvée.")

        return {"order_info_client": order_info_client, "order_delivery_address": order_delivery_address}
    except Exception as e:
        print("Error executing SQL query:", e)
        print("Generated SQL query:", cursor.statement)
        raise HTTPException(status_code=500, detail="Erreur lors de la récupération de la commande")
    finally:
        cursor.close()

