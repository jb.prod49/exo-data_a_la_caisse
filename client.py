# client.py
import mysql.connector
from fastapi import HTTPException, APIRouter
from pydantic import BaseModel
from config.bdd_config import DB_CONFIG

router = APIRouter()

# --------------------------------- #
# --------- Connexion BDD --------- #
# --------------------------------- #

conn = mysql.connector.connect(**DB_CONFIG)

# ------------------------------- #
# --------- Les classes --------- #
# ------------------------------- #

class Client(BaseModel):
    id: int | None = None
    email: str | None = None
    first_name: str | None = None
    last_name: str | None = None
    phone: str | None = None
    default_address: int | None = None

class Address(BaseModel):
    id: int | None = None
    street: str | None = None
    city: str | None = None
    zipcode: int | None = None
    state: str | None = None
    country: str | None = None

# ------------------------------------- #
# -------- creation des routes ---------#
# ------------------------------------- #

# Créer un client
@router.post("/client")
async def create_client(client: Client, adresse: Address):
    cursor = conn.cursor(dictionary=True)

    try:
        cursor.execute("START TRANSACTION")
    # identity
        cursor.execute("""
            INSERT INTO identity (email, first_name, last_name, phone)
            VALUES (%s, %s, %s, %s)
        """, (client.email, client.first_name, client.last_name, client.phone))
        identity_id = cursor.lastrowid

    # address
        default_address = None
        if adresse:
            default_address = await create_address(adresse)
    
    # client
        cursor.execute("""
            INSERT INTO client (id_identity, default_address)
            VALUES (%s, %s)
        """, (identity_id, default_address['id']))
        client_id = cursor.lastrowid
        
    #client_address
        cursor.execute("""
            INSERT INTO client_address (id_client, id_address)
            VALUES (%s, %s)
        """, (client_id, default_address['id']))

        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise HTTPException(status_code=500, detail=f"Erreur lors de la création du client: {str(e)}")
    finally:
        cursor.close()

    return {'client':client,'adresse':adresse}


@router.post("/address")
async def create_address(adresse: Address):
    cursor = conn.cursor(dictionary=True)
    try:
        cursor.execute("START TRANSACTION")
        
        # street
        cursor.execute("""INSERT INTO street (name_street) VALUES (%s);""", 
                       (adresse.street,))
        street_id = cursor.lastrowid
        
        # city, zipcode
        cursor.execute("""INSERT INTO city (name_city, zipcode) VALUES (%s, %s)""", 
                       (adresse.city, adresse.zipcode))
        city_id = cursor.lastrowid

        # state
        cursor.execute("""INSERT INTO state (name_state) VALUES (%s)""", 
                       (adresse.state,))
        state_id = cursor.lastrowid
        
        # country
        cursor.execute("""INSERT INTO country (name_country) VALUES (%s)""", 
                       (adresse.country,))
        country_id = cursor.lastrowid
    
        # address
        cursor.execute("""
            INSERT INTO address (id_street, id_city, id_state, id_country)
            VALUES (%s, %s, %s, %s)
        """, (street_id, city_id, state_id, country_id))
        address_id = cursor.lastrowid
        
        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise HTTPException(status_code=500, detail=f"Erreur lors de la création de l'adresse : {str(e)}")
    finally:
        cursor.close()
        
    return {"id": address_id,"street_id": street_id,"city_id": city_id,"state_id": state_id,"country_id": country_id}


# Afficher les clients avec leurs adresses
@router.get("/client")
async def get_clients():
    cursor = conn.cursor(dictionary=True)
    cursor.execute("""
        SELECT
            c.id AS client_id,
            i.email,
            i.first_name,
            i.last_name,
            i.phone,
            a.id AS address_id,
            s.name_street,
            ct.zipcode,
            ct.name_city,
            st.name_state,
            co.name_country
        FROM
            client c
        JOIN
            identity i ON c.id_identity = i.id
        LEFT JOIN
            address a ON c.default_address = a.id
        LEFT JOIN
            street s ON a.id_street = s.id
        LEFT JOIN
            city ct ON a.id_city = ct.id
        LEFT JOIN
            state st ON a.id_state = st.id
        LEFT JOIN
            country co ON a.id_country = co.id;

    """)
    clients = cursor.fetchall()
    return clients

# Afficher le client grâce à son id
@router.get("/client/{client_id}")
async def get_clients_by_id(client_id: int):
    cursor = conn.cursor(dictionary=True)
    cursor.execute("""
        SELECT
            c.id AS client_id,
            i.email,
            i.first_name,
            i.last_name,
            i.phone,
            a.id AS address_id,
            s.name_street,
            ct.name_city,
            ct.zipcode,
            st.name_state,
            co.name_country
        FROM
            client c
        JOIN
            identity i ON c.id_identity = i.id
        LEFT JOIN
            address a ON c.default_address = a.id
        LEFT JOIN
            street s ON a.id_street = s.id
        LEFT JOIN
            city ct ON a.id_city = ct.id
        LEFT JOIN
            state st ON a.id_state = st.id
        LEFT JOIN
            country co ON a.id_country = co.id
        WHERE c.id = %s
    """, (client_id,))
    clients = cursor.fetchall()
    return clients


# Modifier un client grâce à son id
@router.post("/client/{client_id}")
async def update_client(client_id: int, client: Client, adresse: Address):
    cursor = conn.cursor()
    try:
        cursor.execute("START TRANSACTION")
        print(client)

        update_columns = []
        update_values = []

        # identity
        if client.email is not None:
            update_columns.append("email = %s")
            update_values.append(client.email)

        if client.first_name is not None:
            update_columns.append("first_name = %s")
            update_values.append(client.first_name)

        if client.last_name is not None:
            update_columns.append("last_name = %s")
            update_values.append(client.last_name)

        if client.phone is not None:
            update_columns.append("phone = %s")
            update_values.append(client.phone)
        
        update_query = f"""
            UPDATE identity
            SET {', '.join(update_columns)}
            WHERE id = (SELECT id_identity FROM client WHERE id = %s)
        """
        update_values.append(client_id)
        cursor.execute(update_query, tuple(update_values))
        
        # address
        if adresse:
            default_address = await create_address(adresse)
            address_id = default_address['id']
            
            # Vérifier si le client existe
            cursor.execute("SELECT id FROM client WHERE id = %s", (client_id,))
            client_exists = cursor.fetchone()
            
            if client_exists:
                # Mettre à jour la table client avec l'adresse par défaut
                cursor.execute("""UPDATE client SET default_address = %s WHERE id = %s""", (address_id, client_id))
                
                # client_address
                cursor.execute("""INSERT INTO client_address (id_client, id_address) VALUES (%s, %s)""", (client_id, address_id))
            else:
                raise HTTPException(status_code=404, detail=f"Client with ID {client_id} not found.")
            
        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise e
    finally:
        cursor.close()
    return {"message": "Client updated successfully"}

#Retourne la liste des adresses associées à un client
@router.get("/client/{client_id}/adresses")
async def get_adresses_client(client_id: int):
    cursor = conn.cursor(dictionary=True)
    try:
        cursor.execute("""
            SELECT
                a.id AS address_id,
                s.name_street,
                ct.name_city,
                ct.zipcode,
                st.name_state,
                co.name_country
            FROM
                client_address ca
            JOIN
                address a ON ca.id_address = a.id
            LEFT JOIN
                street s ON a.id_street = s.id
            LEFT JOIN
                city ct ON a.id_city = ct.id
            LEFT JOIN
                state st ON a.id_state = st.id
            LEFT JOIN
                country co ON a.id_country = co.id
            WHERE
                ca.id_client = %s
        """, (client_id,))

        addresses = cursor.fetchall()

        if not addresses:
            raise HTTPException(status_code=404, detail="Aucune adresse trouvée pour ce client.")

        return addresses
    finally:
        cursor.close()


# Supprime les information personnel du client.
@router.delete("/client/{client_id}")
async def delete_product(client_id: int):
    cursor = conn.cursor()
    identity_id = None
    try:
        cursor.execute("START TRANSACTION")
        cursor.execute("""SELECT id_identity FROM client WHERE id = (%s)""", (client_id,))
        row = cursor.fetchone()
        
        if row:
            identity_id = row[0]
            cursor.execute("DELETE FROM identity WHERE id = %s", (identity_id,))
            cursor.execute("UPDATE client SET id_identity = NULL WHERE id = %s", (client_id,))
            
        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise e
    finally:
        cursor.close()

    return {"deleted_client_id": client_id, "deleted_identity_id": identity_id}