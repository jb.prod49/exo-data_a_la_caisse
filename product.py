# product.py
import mysql.connector
from fastapi import HTTPException, APIRouter
from datetime import datetime
from pydantic import BaseModel
from config.bdd_config import DB_CONFIG

router = APIRouter()

# --------------------------------- #
# --------- Connexion BDD --------- #
# --------------------------------- #

conn = mysql.connector.connect(**DB_CONFIG)

# ------------------------------- #
# --------- Les classes --------- #
# ------------------------------- #

class Product(BaseModel):
    id: int | None = None
    id_name: str | None = None
    id_describ: str | None = None
    id_image: str | None = None
    id_price: float | None = None
    id_available: int | None = None

# ------------------------------------- #
# -------- creation des routes ---------#
# ------------------------------------- #

# Créer un produit
@router.post("/product")
async def create_product(product: Product):
    cursor = conn.cursor(dictionary=True)

    try:
        cursor.execute("START TRANSACTION")
        cursor.execute("INSERT INTO name (name) VALUES (%s)", (product.id_name,))
        name_id = cursor.lastrowid
        cursor.execute("INSERT INTO describ (description) VALUES (%s)", (product.id_describ,))
        describ_id = cursor.lastrowid
        cursor.execute("INSERT INTO image (link_img) VALUES (%s)", (product.id_image,))
        image_id = cursor.lastrowid
        cursor.execute("INSERT INTO price (price) VALUES (%s)", (product.id_price,))
        price_id = cursor.lastrowid
        cursor.execute("INSERT INTO product (id_name, id_describ, id_image, id_price, id_available) VALUES (%s, %s, %s, %s, %s)",
               (name_id, describ_id, image_id, price_id, product.id_available))
        product.id = cursor.lastrowid
        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise HTTPException(status_code=500, detail=f"Erreur lors de la création du produit: {str(e)}")
    finally:
        cursor.close()

    return product

#afficher les produits
@router.get("/product")
async def get_products():
    cursor = conn.cursor(dictionary=True)

    try:
        cursor.execute("""
            SELECT
                p.id AS product_id,
                n.name AS product_name,
                d.description AS product_description,
                i.link_img AS product_image,
                pr.price AS product_price
            FROM
                product p
            JOIN
                name n ON p.id_name = n.id
            JOIN
                describ d ON p.id_describ = d.id
            JOIN
                image i ON p.id_image = i.id
            JOIN
                price pr ON p.id_price = pr.id
        """)
        products = cursor.fetchall()
        return products
    finally:
        cursor.close()

# Afficher le produit grâce à son id
@router.get("/product/{product_id}")
async def get_product_by_id(product_id: int):
    cursor = conn.cursor(dictionary=True)
    cursor.execute("""
        SELECT
            p.id AS product_id,
            n.name AS product_name,
            d.description AS product_description,
            i.link_img AS product_image,
            pr.price AS product_price
        FROM
            product p
        JOIN
            name n ON p.id_name = n.id
        JOIN
            describ d ON p.id_describ = d.id
        JOIN
            image i ON p.id_image = i.id
        JOIN
            price pr ON p.id_price = pr.id
        WHERE
            p.id = %s
    """, (product_id,))
    product = cursor.fetchone()
    return product

# Modifier un produit grâce à son id
@router.post("/product/{product_id}")
async def update_product(product_id: int, product: Product):
    cursor = conn.cursor()
    try:
        cursor.execute("START TRANSACTION")
        if product.id_name:
            cursor.execute("INSERT INTO name (name, date_modification) VALUES (%s, %s)", (product.id_name, datetime.now()))
            new_name_id = cursor.lastrowid
            cursor.execute("UPDATE product SET id_name = %s WHERE id = %s", (new_name_id, product_id))

        if product.id_describ:
            cursor.execute("INSERT INTO describ (description, date_modification) VALUES (%s, %s)", (product.id_describ, datetime.now()))
            new_describ_id = cursor.lastrowid
            cursor.execute("UPDATE product SET id_describ = %s WHERE id = %s", (new_describ_id, product_id))

        if product.id_image:
            cursor.execute("INSERT INTO image (link_img, date_modification) VALUES (%s, %s)", (product.id_image, datetime.now()))
            new_image_id = cursor.lastrowid
            cursor.execute("UPDATE product SET id_image = %s WHERE id = %s", (new_image_id, product_id))

        if product.id_price:
            cursor.execute("INSERT INTO price (price, date_modification) VALUES (%s, %s)", (product.id_price, datetime.now()))
            new_price_id = cursor.lastrowid
            cursor.execute("UPDATE product SET id_price = %s WHERE id = %s", (new_price_id, product_id))

        if product.id_available is not None:
            cursor.execute("INSERT INTO availability (id_product, id_available, date_modification) VALUES (%s, %s, %s)",
                           (product_id, product.id_available, datetime.now()))
            cursor.execute("INSERT INTO available (available) VALUES (%s)", (product.id_available,))
            new_available_id = cursor.lastrowid
            cursor.execute("UPDATE product SET id_available = %s WHERE id = %s", (product.id_available, product_id))
        cursor.execute("COMMIT")

    except Exception as e:
        cursor.execute("ROLLBACK")
        raise e
    finally:
        cursor.close()
    return {"message": "Product updated successfully"}

# Supprime le produit grâce a son ID
@router.delete("/product/{product_id}")
async def delete_product(product_id: int):
    cursor = conn.cursor()
    try:
        cursor.execute("START TRANSACTION")
        cursor.execute("DELETE FROM availability WHERE id_product = %s", (product_id,))
        cursor.execute("DELETE FROM product WHERE id = %s", (product_id,))
        cursor.execute("COMMIT")
    except Exception as e:
        cursor.execute("ROLLBACK")
        raise e
    finally:
        cursor.close()

    return {"message": "Product deleted successfully"}
