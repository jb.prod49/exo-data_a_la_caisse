# main.py
from fastapi import FastAPI
from product import router as product_router
from client import router as client_router
from order import router as order_router

app = FastAPI(debug=True)

app.include_router(product_router)
app.include_router(client_router)
app.include_router(order_router)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)