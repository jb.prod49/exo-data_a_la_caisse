-- Dropping the existing database if exists
DROP DATABASE IF EXISTS la_caisse;

-- Creating the new database
CREATE DATABASE la_caisse;

-- Switching to the new database
USE la_caisse;

-- Création des tables de référence
CREATE TABLE client (
  id INT AUTO_INCREMENT PRIMARY KEY,
  default_address INT,
  id_identity INT UNIQUE
);

CREATE TABLE identity (
  id INT AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(255),
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  phone VARCHAR(20)
);

CREATE TABLE address (
  id INT AUTO_INCREMENT PRIMARY KEY,
  id_street INT,
  id_city INT,
  id_state INT,
  id_country INT
);

CREATE TABLE street (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name_street VARCHAR(255)
);

CREATE TABLE city (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name_city VARCHAR(255),
  zipcode VARCHAR(20)
);

CREATE TABLE state (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name_state VARCHAR(255)
);

CREATE TABLE country (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name_country VARCHAR(255)
);

CREATE TABLE product (
  id INT AUTO_INCREMENT PRIMARY KEY,
  id_name INT,
  id_describ INT,
  id_image INT,
  id_price INT,
  id_available INT
);

CREATE TABLE name (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  date_modification TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE describ (
  id INT AUTO_INCREMENT PRIMARY KEY,
  description TEXT,
  date_modification TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE image (
  id INT AUTO_INCREMENT PRIMARY KEY,
  link_img VARCHAR(255),
  date_modification TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE price (
  id INT AUTO_INCREMENT PRIMARY KEY,
  price DECIMAL(10, 2),
  date_modification TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE available (
  id INT AUTO_INCREMENT PRIMARY KEY,
  available BOOLEAN
);

CREATE TABLE availability (
  id_product INT,
  id_available INT,
  date_modification TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE orders (
  id INT AUTO_INCREMENT PRIMARY KEY,
  id_client INT,
  delivery_address INT,
  billing_address INT,
  payment_method ENUM ('None','credit_card', 'bank_check_on_delivery', 'cash'),
  order_state ENUM ('cart', 'validated', 'sent', 'delivered'),
  date_statut TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (id_client) REFERENCES client (id),
  FOREIGN KEY (delivery_address) REFERENCES address (id),
  FOREIGN KEY (billing_address) REFERENCES address (id)
);

CREATE TABLE order_line (
  id_product INT,
  id_order INT,
  quantity INT,
  FOREIGN KEY (id_product) REFERENCES product (id),
  FOREIGN KEY (id_order) REFERENCES orders (id)
);

CREATE TABLE client_address (
  id_client INT,
  id_address INT,
  date_association TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (id_client) REFERENCES client (id),
  FOREIGN KEY (id_address) REFERENCES address (id)
);
